from django.urls import path

from src.views import *

urlpatterns = [
    path('age', StaffAgeStatistics.as_view()),
    path('age-dynamics', StaffAgeGroupStatistics.as_view()),
    path('language', LanguageProficiencyAPIView.as_view()),
    path('position', PositionAPIView.as_view()),
    path('nationalities', NationalityAPIView.as_view()),
    path('gender', GenderAPIView.as_view()),
    path('education', EducationLevelAPIView.as_view()),
    path('transfer', StaffTransferAPIView.as_view()),
    path('dismissal', StaffLeavingReasons.as_view()),
    path('age-gender', StaffAgeGroupByGenderAPIView.as_view()),
    path('working-expirence', StaffWorkingExperienceAPIView.as_view()),
    path('employee', EmployeeAPIView.as_view()),
    path('employee-department', StaffCountByDepartment.as_view()),
    path('employee-department-age', StaffAgeRangeByDepartmentAPIView.as_view()),
    path('employee-expirence', StaffWorkingYearsAPIView.as_view()),
    path('employee-expirence', StaffWorkingYearsAPIView.as_view()),

    path('employee-termination', StaffTerminationAPIView.as_view()),
    path('employee-termination-month', StaffTerminationMonthAPIView.as_view()),
    path('employee-termination-reason', StaffTerminationReasonAPIView.as_view()),

]
