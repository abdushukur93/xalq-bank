import calendar
import os

import pandas as pd
from rest_framework import status
from rest_framework.exceptions import APIException
from rest_framework.response import Response
from rest_framework.views import APIView

from src.pagination import CustomPagination


class StaffAgeStatistics(APIView):
    def get(self, request, *args, **kwargs):
        base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        try:
            filepath_staff = os.path.join(base_dir, 'files', 'xalq-bank.xlsx')
            if not os.path.isfile(filepath_staff):
                raise APIException("Excel file 'xalq-bank.xlsx' not found.")

            filepath_dismissed = os.path.join(base_dir, 'files', '2020-2024 ishga qabul qilingan va olinganlar.xlsx')
            if not os.path.isfile(filepath_dismissed):
                raise APIException("Excel file '2020-2024 ishga qabul qilingan va olinganlar.xlsx' not found.")

            df_staff = pd.read_excel(filepath_staff)

            staff_count = len(df_staff)

            average_age = df_staff['Age'].mean()

            df_dismissed = pd.read_excel(filepath_dismissed)

            dismissed_count = df_dismissed[df_dismissed['Date of dismissal'].notna()].shape[0]

            response_data = {
                'staff_count': staff_count,
                'average_age': round(average_age),
                'dismissed_count': dismissed_count
            }

            return Response(response_data)

        except FileNotFoundError:
            return Response({"error": "Excel file not found"}, status=status.HTTP_404_NOT_FOUND)


class StaffAgeGroupStatistics(APIView):
    def get(self, request, *args, **kwargs):
        base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        try:
            filepath = os.path.join(base_dir, 'files', 'xalq-bank.xlsx')
            if not os.path.isfile(filepath):
                raise APIException("Excel file not found.")

            df = pd.read_excel(filepath)

            all_age_groups = {
                '20-30': len(df[df['Age'] >= 20 & (df['Age'] < 30)]),
                '30-40': len(df[(df['Age'] >= 30) & (df['Age'] < 40)]),
                '40-50': len(df[(df['Age'] >= 40) & (df['Age'] < 50)]),
                '50-60': len(df[(df['Age'] >= 50) & (df['Age'] < 60)]),
                '60 and higher': len(df[df['Age'] >= 60]),
            }

            male_df = df[df['Sex'] == 'MALE']
            male_age_groups = {
                '20-30': len(male_df[male_df['Age'] >= 20 & (male_df['Age'] < 30)]),
                '30-40': len(male_df[(male_df['Age'] >= 30) & (male_df['Age'] < 40)]),
                '40-50': len(male_df[(male_df['Age'] >= 40) & (male_df['Age'] < 50)]),
                '50-60': len(male_df[(male_df['Age'] >= 50) & (male_df['Age'] < 60)]),
                '60 and higher': len(male_df[male_df['Age'] >= 60]),
            }

            female_df = df[df['Sex'] == 'WOMAN']
            female_age_groups = {
                '20-30': len(female_df[female_df['Age'] >= 20 & (female_df['Age'] < 30)]),
                '30-40': len(female_df[(female_df['Age'] >= 30) & (female_df['Age'] < 40)]),
                '40-50': len(female_df[(female_df['Age'] >= 40) & (female_df['Age'] < 50)]),
                '50-60': len(female_df[(female_df['Age'] >= 50) & (female_df['Age'] < 60)]),
                '60 and higher': len(female_df[female_df['Age'] >= 60]),
            }

            response_data = {
                'age_groups': all_age_groups,
                'male': male_age_groups,
                'woman': female_age_groups
            }

            return Response(response_data)

        except FileNotFoundError:
            return Response({"error": "Excel file not found"}, status=status.HTTP_404_NOT_FOUND)


class LanguageProficiencyAPIView(APIView):
    def get(self, request, *args, **kwargs):
        base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        try:
            filepath = os.path.join(base_dir, 'files', 'xalq-bank.xlsx')
            if not os.path.isfile(filepath):
                raise APIException("Excel file not found.")

            df = pd.read_excel(filepath)

            all_counts = df['Language knowledge in general (for analysis)'].value_counts()

            male_df = df[df['Sex'] == 'MALE']
            male_counts = male_df['Language knowledge in general (for analysis)'].value_counts()

            female_df = df[df['Sex'] == 'WOMAN']
            female_counts = female_df['Language knowledge in general (for analysis)'].value_counts()

            response_data = {
                'default': self._process_language_proficiency(all_counts),
                'male': self._process_language_proficiency(male_counts),
                'woman': self._process_language_proficiency(female_counts)
            }

            return Response(response_data)

        except FileNotFoundError:
            return Response({"error": "Excel file not found"}, status=status.HTTP_404_NOT_FOUND)

    def _process_language_proficiency(self, counts):
        total = counts.sum()
        language_data = {}
        for language, count in counts.items():
            language_data[language] = {
                'count': count,
                'percent': round((count / total) * 100, 2) if total != 0 else 0
            }
        return language_data


class EducationLevelAPIView(APIView):
    def get(self, request, *args, **kwargs):
        base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        try:
            filepath = os.path.join(base_dir, 'files', 'xalq-bank.xlsx')
            if not os.path.isfile(filepath):
                raise APIException("Excel file not found.")

            df = pd.read_excel(filepath)

            all_counts = df['Information'].value_counts()

            male_df = df[df['Sex'] == 'MALE']
            male_counts = male_df['Information'].value_counts()

            female_df = df[df['Sex'] == 'WOMAN']
            female_counts = female_df['Information'].value_counts()

            response_data = {
                'default': self._process_education_level_distribution(all_counts),
                'male': self._process_education_level_distribution(male_counts),
                'female': self._process_education_level_distribution(female_counts)
            }

            return Response(response_data)

        except FileNotFoundError:
            return Response({"error": "Excel file not found"}, status=status.HTTP_404_NOT_FOUND)

    def _process_education_level_distribution(self, counts):
        total = counts.sum()
        distribution_data = {}
        for level, count in counts.items():
            distribution_data[level] = {
                'count': count,
                'percent': round((count / total) * 100, 2) if total != 0 else 0
            }
        return distribution_data


class NationalityAPIView(APIView):
    def get(self, request, *args, **kwargs):
        base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        try:
            filepath = os.path.join(base_dir, 'files', 'xalq-bank.xlsx')
            if not os.path.isfile(filepath):
                raise APIException("Excel file not found.")

            df = pd.read_excel(filepath)

            all_counts, all_percentages = self._process_nationality_data(df)

            male_df = df[df['Sex'] == 'MALE']
            male_counts, male_percentages = self._process_nationality_data(male_df)

            female_df = df[df['Sex'] == 'WOMAN']
            female_counts, female_percentages = self._process_nationality_data(female_df)

            response_data = {
                'default': self._format_response(all_counts, all_percentages),
                'male': self._format_response(male_counts, male_percentages),
                'female': self._format_response(female_counts, female_percentages)
            }

            return Response(response_data)

        except FileNotFoundError:
            return Response({"error": "Excel file not found"}, status=status.HTTP_404_NOT_FOUND)

    def _process_nationality_data(self, df):
        counts = df['Nationality analysis'].value_counts().to_dict()
        total_count = sum(counts.values())
        percentages = {nationality: (count / total_count) * 100 for nationality, count in counts.items()}
        return counts, percentages

    def _format_response(self, counts, percentages):
        formatted_response = {}
        for nationality, count in counts.items():
            formatted_response[nationality] = {
                'count': count,
                'percent': percentages[nationality]
            }
        return formatted_response


class GenderAPIView(APIView):
    def get(self, request, *args, **kwargs):
        base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        try:
            filepath = os.path.join(base_dir, 'files', 'xalq-bank.xlsx')
            if not os.path.isfile(filepath):
                raise APIException("Excel file not found.")

            df = pd.read_excel(filepath)

            male_count = len(df[df['Sex'] == 'MALE'])
            female_count = len(df[df['Sex'] == 'WOMAN'])
            total_count = male_count + female_count

            male_percentage = round((male_count / total_count) * 100, 2) if total_count != 0 else 0
            female_percentage = round((female_count / total_count) * 100, 2) if total_count != 0 else 0

            response_data = {
                'male': {
                    'count': male_count,
                    'percentage': male_percentage
                },
                'woman': {
                    'count': female_count,
                    'percentage': female_percentage
                }
            }

            return Response(response_data)

        except FileNotFoundError:
            return Response({"error": "Excel file not found"}, status=status.HTTP_404_NOT_FOUND)


class PositionAPIView(APIView):
    def get(self, request, *args, **kwargs):
        base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        try:
            filepath = os.path.join(base_dir, 'files', 'xalq-bank.xlsx')
            if not os.path.isfile(filepath):
                raise APIException("Excel file not found.")

            df = pd.read_excel(filepath)

            all_counts = df['Position'].value_counts()
            total_count = all_counts.sum()
            all_percentages = {position: round((count / total_count) * 100, 2) for position, count in
                               all_counts.items()}

            male_df = df[df['Sex'] == 'MALE']
            male_counts = male_df['Position'].value_counts()
            male_total_count = male_counts.sum()
            male_percentages = {position: round((count / male_total_count) * 100, 2) for position, count in
                                male_counts.items()}

            female_df = df[df['Sex'] == 'WOMAN']
            female_counts = female_df['Position'].value_counts()
            female_total_count = female_counts.sum()
            female_percentages = {position: round((count / female_total_count) * 100, 2) for position, count in
                                  female_counts.items()}

            response_data = {
                'default': {position: {'count': count, 'percent': all_percentages[position]} for position, count in
                            all_counts.items()},
                'male': {position: {'count': count, 'percent': male_percentages[position]} for position, count in
                         male_counts.items()},
                'female': {position: {'count': count, 'percent': female_percentages[position]} for position, count in
                           female_counts.items()}
            }

            return Response(response_data)

        except FileNotFoundError:
            return Response({"error": "Excel file not found"}, status=status.HTTP_404_NOT_FOUND)


class StaffLeavingReasons(APIView):
    def get(self, request, *args, **kwargs):
        base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        try:
            filepath = os.path.join(base_dir, 'files', '2020-2024 ishga qabul qilingan va olinganlar.xlsx')
            if not os.path.isfile(filepath):
                raise APIException("Excel file not found.")

            df = pd.read_excel(filepath)

            df['Date of dismissal'] = pd.to_datetime(df['Date of dismissal'], errors='coerce')

            df['Year'] = df['Date of dismissal'].dt.year

            df = df.dropna(subset=['Year'])
            df['Year'] = df['Year'].astype(int)

            reasons_by_year = df.groupby('Year')['Reason for dismissal'].value_counts().unstack(fill_value=0)

            reasons_data = reasons_by_year.to_dict()

            response_data = {
                'reasons_by_year': reasons_data
            }

            return Response(response_data)

        except FileNotFoundError:
            return Response({"error": "Excel file not found"}, status=status.HTTP_404_NOT_FOUND)


class StaffTransferAPIView(APIView):
    def get(self, request, *args, **kwargs):
        base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        try:
            filepath = os.path.join(base_dir, 'files', 'Boshqa lavozimga o\'tgan2.xlsx')
            if not os.path.isfile(filepath):
                raise APIException("Excel file not found.")

            df = pd.read_excel(filepath)

            df['Янги лавозимга ўтган санаси'] = pd.to_datetime(df['Янги лавозимга ўтган санаси'], format='%d.%m.%Y')

            df['Year'] = df['Янги лавозимга ўтган санаси'].dt.year

            df['Year'] = df['Year'].fillna(0)

            df['Year'] = df['Year'].astype(int)

            transfer_counts = df['Year'].value_counts().sort_index()
            total_transfers = transfer_counts.sum()
            transfer_percentages = (transfer_counts / total_transfers) * 100

            transfer_data = {
                'counts': transfer_counts.to_dict(),
                'percentages': transfer_percentages.to_dict()
            }

            transfer_data['counts'].pop(0, None)
            transfer_data['percentages'].pop(0, None)

            return Response(transfer_data)

        except FileNotFoundError:
            return Response({"error": "Excel file not found"}, status=status.HTTP_404_NOT_FOUND)


class StaffAgeGroupByGenderAPIView(APIView):
    def get(self, request, *args, **kwargs):
        base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        try:
            filepath = os.path.join(base_dir, 'files', 'xalq-bank.xlsx')
            if not os.path.isfile(filepath):
                raise APIException("Excel file not found.")

            df = pd.read_excel(filepath)

            male_df = df[df['Sex'] == 'MALE']
            female_df = df[df['Sex'] == 'WOMAN']

            male_age_groups = {
                '20-30': len(male_df[(male_df['Age'] >= 20) & (male_df['Age'] < 30)]),
                '30-40': len(male_df[(male_df['Age'] >= 30) & (male_df['Age'] < 40)]),
                '40-50': len(male_df[(male_df['Age'] >= 40) & (male_df['Age'] < 50)]),
                '50-60': len(male_df[(male_df['Age'] >= 50) & (male_df['Age'] < 60)]),
                '60 and higher': len(male_df[male_df['Age'] >= 60])
            }

            female_age_groups = {
                '20-30': len(female_df[(female_df['Age'] >= 20) & (female_df['Age'] < 30)]),
                '30-40': len(female_df[(female_df['Age'] >= 30) & (female_df['Age'] < 40)]),
                '40-50': len(female_df[(female_df['Age'] >= 40) & (female_df['Age'] < 50)]),
                '50-60': len(female_df[(female_df['Age'] >= 50) & (female_df['Age'] < 60)]),
                '60 and higher': len(female_df[female_df['Age'] >= 60])
            }

            min_age_by_position = df.groupby('Position')['Age'].min()
            max_age_by_position = df.groupby('Position')['Age'].max()

            min_age_by_position = min_age_by_position.to_dict()
            max_age_by_position = max_age_by_position.to_dict()

            age_range_by_position = {position: {'min_age': min_age, 'max_age': max_age}
                                     for position, min_age, max_age in zip(min_age_by_position.keys(),
                                                                           min_age_by_position.values(),
                                                                           max_age_by_position.values())}

            response_data = {
                'male': male_age_groups,
                'female': female_age_groups,
                'male_count': len(male_df),
                'female_count': len(female_df),
                'male_percent': round((len(male_df) / len(df)) * 100, 2),
                'female_percent': round((len(female_df) / len(df)) * 100, 2),
                'age_range_by_position': age_range_by_position
            }

            return Response(response_data)


        except FileNotFoundError:
            return Response({"error": "Excel file not found"}, status=status.HTTP_404_NOT_FOUND)


class StaffWorkingExperienceAPIView(APIView):
    def get(self, request, *args, **kwargs):
        base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        try:
            filepath = os.path.join(base_dir, 'files', 'xalq-bank.xlsx')
            if not os.path.isfile(filepath):
                raise APIException("Excel file not found.")

            df = pd.read_excel(filepath)

            # Extract the required columns
            working_experience_df = df[
                ['General', 'In the banking system', 'In the same banking system', 'In position']]

            # Convert the dataframe to a dictionary
            working_experience_data = working_experience_df.to_dict(orient='records')

            return Response(working_experience_data)

        except FileNotFoundError:
            return Response({"error": "Excel file not found"}, status=status.HTTP_404_NOT_FOUND)


class EmployeeAPIView(APIView):
    pagination_class = CustomPagination

    def get(self, request, *args, **kwargs):
        base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        try:
            filepath = os.path.join(base_dir, 'files', 'xalq-bank.xlsx')
            if not os.path.isfile(filepath):
                raise APIException("Excel file not found.")

            df = pd.read_excel(filepath)

            employee_df = df[
                ['Born_date', 'Position', 'Information', 'Language knowledge in general (for analysis)', 'Sex', 'Age',
                 'Nationality analysis']]

            employee_data = employee_df.to_dict(orient='records')

            paginator = CustomPagination()
            paginated_employee_data = paginator.paginate_queryset(employee_data, request)
            return paginator.get_paginated_response(paginated_employee_data)

        except FileNotFoundError:
            return Response({"error": "Excel file not found"}, status=status.HTTP_404_NOT_FOUND)


class StaffCountByDepartment(APIView):
    def get(self, request, *args, **kwargs):
        base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        try:
            filepath = os.path.join(base_dir, 'files', 'xalq-bank.xlsx')
            if not os.path.isfile(filepath):
                raise APIException("Excel file not found.")

            df = pd.read_excel(filepath)

            staff_count_by_department = df.groupby('Departament').size()

            staff_count_by_department_df = staff_count_by_department.reset_index(name='StaffCount')

            staff_count_by_department_data = staff_count_by_department_df.to_dict(orient='records')

            return Response(staff_count_by_department_data)

        except FileNotFoundError:
            return Response({"error": "Excel file not found"}, status=status.HTTP_404_NOT_FOUND)


class StaffAgeRangeByDepartmentAPIView(APIView):
    def get(self, request, *args, **kwargs):
        base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        try:
            filepath_xalq_bank = os.path.join(base_dir, 'files', 'xalq-bank.xlsx')
            filepath_department_position = os.path.join(base_dir, 'files', 'department_position.xls')
            if not os.path.isfile(filepath_xalq_bank) or not os.path.isfile(filepath_department_position):
                raise APIException("Excel file not found.")

            df_xalq_bank = pd.read_excel(filepath_xalq_bank)
            df_department_position = pd.read_excel(filepath_department_position)

            merged_df = pd.merge(df_xalq_bank, df_department_position, on='Position')

            min_age_by_department = merged_df.groupby('Department')['Age'].min()
            max_age_by_department = merged_df.groupby('Department')['Age'].max()

            min_age_by_department_df = min_age_by_department.reset_index(name='MinAge')
            max_age_by_department_df = max_age_by_department.reset_index(name='MaxAge')

            age_range_by_department_df = pd.merge(min_age_by_department_df, max_age_by_department_df, on='Department')

            age_range_by_department_data = age_range_by_department_df.to_dict(orient='records')

            return Response(age_range_by_department_data)

        except FileNotFoundError:
            return Response({"error": "Excel file not found"}, status=status.HTTP_404_NOT_FOUND)


from datetime import datetime


class StaffWorkingYearsAPIView(APIView):
    def get(self, request, *args, **kwargs):
        base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        try:
            filepath = os.path.join(base_dir, 'files', 'xalq-bank.xlsx')
            if not os.path.isfile(filepath):
                raise APIException("Excel file not found.")

            df = pd.read_excel(filepath)

            current_year = datetime.now().year

            df['In the same banking system'] = pd.to_datetime(df['In the same banking system']).dt.year
            df['In the same banking system'] = current_year - df['In the same banking system']
            employee_counts = df['In the same banking system'].value_counts().sort_index().to_dict()

            grouped_counts = {
                '1 year': employee_counts.get(1, 0),
                '2 years': employee_counts.get(2, 0),
                '3 years': employee_counts.get(3, 0),
                '4 years': employee_counts.get(4, 0),
                '5 years': employee_counts.get(5, 0),
                '6 years': employee_counts.get(6, 0),
                '7 years': employee_counts.get(7, 0),
                '8 years': employee_counts.get(8, 0),
                '9 years': employee_counts.get(9, 0),
                '10 years': employee_counts.get(10, 0),
                '11 years': employee_counts.get(11, 0),
                '12 years': employee_counts.get(12, 0),
                '13 years': employee_counts.get(13, 0),
                '14 years': employee_counts.get(14, 0),
                '15 years': employee_counts.get(15, 0),
                '16 years': employee_counts.get(16, 0),
                '17 years': employee_counts.get(17, 0),
                '18 years': employee_counts.get(18, 0),
                '19 years': employee_counts.get(19, 0),
                '20 more years': employee_counts.get(20, 0)
            }

            return Response(grouped_counts)

        except FileNotFoundError:
            return Response({"error": "Excel file not found"}, status=status.HTTP_404_NOT_FOUND)


class StaffTerminationAPIView(APIView):
    def get(self, request, *args, **kwargs):
        base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        try:
            filepath = os.path.join(base_dir, 'files', '2020-2024 ishga qabul qilingan va olinganlar.xlsx')
            if not os.path.isfile(filepath):
                raise APIException("Excel file not found.")

            df = pd.read_excel(filepath)

            df['Date of dismissal'] = pd.to_datetime(df['Date of dismissal'], format="%d.%m.%Y")

            df['Year'] = df['Date of dismissal'].dt.year
            df['Month'] = df['Date of dismissal'].dt.month

            termination_counts = df.groupby(['Year', 'Month']).size().reset_index(name='Count')

            termination_counts_dict = {}
            for _, row in termination_counts.iterrows():
                year = int(row['Year'])
                month = calendar.month_name[int(row['Month'])]
                count = int(row['Count'])
                if year not in termination_counts_dict:
                    termination_counts_dict[year] = {}
                termination_counts_dict[year][month] = count

            return Response(termination_counts_dict)

        except FileNotFoundError:
            return Response({"error": "Excel file not found"}, status=status.HTTP_404_NOT_FOUND)


class StaffTerminationMonthAPIView(APIView):
    def get(self, request, *args, **kwargs):
        base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        try:
            filepath = os.path.join(base_dir, 'files', '2020-2024 ishga qabul qilingan va olinganlar.xlsx')
            if not os.path.isfile(filepath):
                raise APIException("Excel file not found.")

            df = pd.read_excel(filepath)

            df['Date of dismissal'] = pd.to_datetime(df['Date of dismissal'], format="%d.%m.%Y")

            df['Month'] = df['Date of dismissal'].dt.month

            termination_counts = df.groupby('Month').size().reset_index(name='Count')

            termination_counts_dict = {}
            for _, row in termination_counts.iterrows():
                month = calendar.month_name[int(row['Month'])]
                count = int(row['Count'])
                termination_counts_dict[month] = count

            return Response(termination_counts_dict)

        except FileNotFoundError:
            return Response({"error": "Excel file not found"}, status=status.HTTP_404_NOT_FOUND)


class StaffTerminationReasonAPIView(APIView):
    def get(self, request, *args, **kwargs):
        base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        try:
            filepath = os.path.join(base_dir, 'files', '2020-2024 ishga qabul qilingan va olinganlar.xlsx')
            if not os.path.isfile(filepath):
                raise APIException("Excel file not found.")

            df = pd.read_excel(filepath)

            termination_reason_counts = df['Reason for dismissal'].value_counts().reset_index()

            return Response(termination_reason_counts.to_dict(orient='records'))

        except FileNotFoundError:
            return Response({"error": "Excel file not found"}, status=status.HTTP_404_NOT_FOUND)
