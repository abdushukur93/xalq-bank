from django.db.models import *


class Map(Model):
    name = CharField(max_length=100, null=True, blank=True)
    hour = CharField(max_length=100, null=True, blank=True)
    type = CharField(max_length=100, null=True, blank=True)
    latitude = DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)
    longitude = DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)


class Staffs(Model):
    full_name = CharField(max_length=255, null=True, blank=True)
    birh_day = CharField(max_length=255, null=True, blank=True)
    position = CharField(max_length=255, null=True, blank=True)
    general_experince = CharField(max_length=255, null=True, blank=True)
    bank_system = CharField(max_length=255, null=True, blank=True)
    same_bank_system = CharField(max_length=255, null=True, blank=True)
    in_position = CharField(max_length=255, null=True, blank=True)
    info = CharField(max_length=255, null=True, blank=True)
    language = CharField(max_length=255, null=True, blank=True)
    gender = CharField(max_length=255, null=True, blank=True)
    age = CharField(max_length=255, null=True, blank=True)
    sign_number = CharField(max_length=255, null=True, blank=True)
    nation = CharField(max_length=255, null=True, blank=True)
