from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from src.models import Map, Staffs

# @admin.register(Map)
# class RegionModelAdmin(ImportExportModelAdmin):
#     list_display = ("id", "name", "hour", "type", "latitude", "longitude")
#     filter = ("name",)
#     ordering = ("id",)


from django.contrib import admin
from leaflet.admin import LeafletGeoAdmin
from .models import Map


@admin.register(Map)
class MapAdmin(LeafletGeoAdmin):
    list_display = ('name', 'hour', 'type', 'latitude', 'longitude')


@admin.register(Staffs)
class StaffsModelAdmin(ImportExportModelAdmin):
    list_display = ("id", "full_name", "birh_day", "position", "general_experince", "bank_system")

    ordering = ("id",)