from rest_framework import serializers


class CombinedDataSerializer(serializers.Serializer):
    staff_count = serializers.IntegerField()
    age_groups = serializers.DictField(child=serializers.IntegerField())
    average_age = serializers.FloatField()


class NationalitySerializer(serializers.Serializer):
    Jami = serializers.IntegerField()
    Ozbeklar = serializers.IntegerField()
    Ozbeklar_percent = serializers.FloatField()
    Ruslar = serializers.IntegerField()
    Ruslar_percent = serializers.FloatField()
    Tatarlar = serializers.IntegerField()
    Tatarlar_percent = serializers.FloatField()
    Tojiklar = serializers.IntegerField()
    Tojiklar_percent = serializers.FloatField()
    Qozoqlar = serializers.IntegerField()
    Qozoqlar_percent = serializers.FloatField()
    Turkman = serializers.IntegerField()
    Turkman_percent = serializers.FloatField()
    Kirgiz = serializers.IntegerField()
    Kirgiz_percent = serializers.FloatField()
    Koreyc = serializers.IntegerField()
    Koreyc_percent = serializers.FloatField()
    Boshqalar = serializers.IntegerField()
    Boshqalar_percent = serializers.FloatField()
    Jami_xodimlar = serializers.FloatField()



